import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Search from "../screens/Search";

const Stack = createStackNavigator();

function SearchStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="account"
                component={Search}
                options={
                    {
                        title: "Buscador"
                    }
                }
            />
        </Stack.Navigator>
    )
}

export default SearchStack;