import React from "react";
import { StyleSheet, View } from "react-native";
import { Input, Icon, Button } from "react-native-elements";

function RegisterForm() {
    return (
        <View
            style={styles.formContainer}
        >
            <Input
                placeholder="Correo electrónico"
                containerStyle={styles.inputForm}
            />
            <Input
                passwordRules={true}
                secureTextEntry={true}
                placeholder="Contraseña"
                containerStyle={styles.inputForm}
            />
            <Input
                passwordRules={true}
                secureTextEntry={true}
                placeholder="Repetir contraseña"
                containerStyle={styles.inputForm}
            />
            <Button
                title="Registrarse"
                containerStyle={styles.btnContainaerRegister}
                buttonStyle={styles.btnRegister}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    formContainer: {
        /*flex: 1,
        alignItems: "center",
        justifyContent: "center",*/
        marginTop: 30
    },
    inputForm: {
        width: "100%",
        marginTop: 20
    },
    btnContainaerRegister: {
        marginTop: 20,
        width: "95%"
    },
    btnRegister: {
        backgroundColor: "#00a680"
    }
});

export default RegisterForm;