import firebase from "firebase/app";

var firebaseConfig = {
    apiKey: "AIzaSyAkoHhBvfkjmcjijSlnSNXbjL2GrAVr4O4",
    authDomain: "tenedores-30840.firebaseapp.com",
    databaseURL: "https://tenedores-30840.firebaseio.com",
    projectId: "tenedores-30840",
    storageBucket: "tenedores-30840.appspot.com",
    messagingSenderId: "769053225520",
    appId: "1:769053225520:web:c5fac757e35916c4c633f5"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);